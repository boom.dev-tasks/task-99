import react from "react";
import { Card, Form, Input, Button } from "./form.js"

function LoginForm() {
    return (
        <Card>
            <Form>
                <Input className="email" name="email" type='email'></Input>
                <Input className="input" name="password" type='password'></Input>
                <Button className="submit" type="submit">login</Button>
            </Form>
        </Card>
    )
}

export default LoginForm